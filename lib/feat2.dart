import 'package:flutter/material.dart';

class Feature2 extends StatefulWidget {
  const Feature2({Key? key}) : super(key: key);

  @override
  _Feature2State createState() => _Feature2State();
}

class _Feature2State extends State<Feature2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text("Campus News Feed - Feature 2"),
        ),
        body:
            const Center(child: Text("Campus News Feed Feature Coming Soon")));
  }
}
